//
//  GlobalQuoteAPI.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class GlobalQuoteAPI {
    
    let apiService = APIService()
    
    func getGlobalQuoteWithSymbol(_ symbol: String, completion: @escaping (Result<GlobalQuoteResult, Error>) -> ()) {
        let globalQuoteRoute = GlobalQuoteRoute(symbol: symbol)
        self.apiService.request(urlRequestConvertible: globalQuoteRoute, completion: completion)
    }
}
