//
//  GlobalQuoteRoute.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

struct GlobalQuoteRoute: APIRouter {
    
    let symbol: String
    
    var endpoint: String {
        return ""
    }
    
    var path: String {
        return "query"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameterEncoding: ParameterEncoding? {
        return URLEncoding.default
    }
    
    var parameters: Parameters? {
        return [
            "function": "GLOBAL_QUOTE",
            "symbol": symbol,
            "datatype": "json",
            "apikey": Config.API.APIKEY
        ]
    }
}
