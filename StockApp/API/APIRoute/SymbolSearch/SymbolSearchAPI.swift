//
//  SymbolSearchAPI.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class SymbolSearchAPI {
    
    let apiService = APIService()
    
    func symbolSearchWithKeyword(_ keyword: String, completion: @escaping (Result<SymbolSearchResult, Error>) -> ()) {
        let symbolSearchRoute = SymbolSearchRoute(keyword: keyword)
        self.apiService.request(urlRequestConvertible: symbolSearchRoute, completion: completion)
    }
}
