//
//  SymbolSearchRoute.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

struct SymbolSearchRoute: APIRouter {
    
    let keyword: String
    
    var endpoint: String {
        return ""
    }
    
    var path: String {
        return "query"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameterEncoding: ParameterEncoding? {
        return URLEncoding.default
    }
    
    var parameters: Parameters? {
        return [
            "function": "SYMBOL_SEARCH",
            "keywords": keyword,
            "datatype": "json",
            "apikey": Config.API.APIKEY
        ]
    }
}
