//
//  API.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import PMKFoundation
import PromiseKit

protocol APIProtocol: class {
    func request(urlRequestConvertible: URLRequestConvertible) -> Promise<(data: Data, response: URLResponse)>
}

class API: APIProtocol {
    
    func request(urlRequestConvertible: URLRequestConvertible) -> Promise<(data: Data, response: URLResponse)> {
        return URLSession.shared.dataTask(.promise, with: urlRequestConvertible)
    }
}
