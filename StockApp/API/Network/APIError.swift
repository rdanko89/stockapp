//
//  APIError.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

enum APIError: Error {
    
    case customError(error: JSONModel)
    case parameterEncodingFailed(reason: ParameterEncodingFailureReason)
    case jsonParseFailure
    
    enum ParameterEncodingFailureReason {
        case missingURL
        case jsonEncodingFailed(error: Error)
    }
}

struct CustomError: JSONModel, Hashable {
    
    var message: String
}
