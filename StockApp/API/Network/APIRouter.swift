//
//  APIRouter.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import PMKFoundation

protocol APIRouter: URLRequestConvertible {
    
    static var baseURL: URL { get }
    var endpoint: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var parameterEncoding: ParameterEncoding? { get }
    
    func getUrlRequest() -> URLRequest
}

extension APIRouter {
    
    static var baseURL: URL { return URL(string: Config.API.baseURLString)! }
    
    var pmkRequest: URLRequest { return self.getUrlRequest() }
    
    func getUrlRequest() -> URLRequest {

        let url = Self.baseURL
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = self.method.rawValue
        
        if let parameters = self.parameters, let encoding = self.parameterEncoding {
            
            do {
                urlRequest = try encoding.encode(urlRequest, with: parameters)
            } catch {
                fatalError("Something wrong, please check at: \(urlRequest)")
            }
        }
        
        return urlRequest
    }
}
