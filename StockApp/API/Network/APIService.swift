//
//  APIService.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import PMKFoundation
import PromiseKit

class APIService {
    
    private let api: APIProtocol
    private let parser = JSONParser()
    
    init(api: APIProtocol = API()) {
        self.api = api
    }
    
    func request<T: JSONModel>(urlRequestConvertible: URLRequestConvertible, completion: @escaping (Swift.Result<T, Error>) -> ()) {
        
        firstly {
            self.api.request(urlRequestConvertible: urlRequestConvertible)
        }.then { (data, response) in
            self.parser.printResponseJSON(data: data)
        }.then { data in
            self.parser.parse(type: T.self, data: data)
        }.done { result in
            completion(.success(result))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func request<T: JSONModel>(urlRequestConvertible: URLRequestConvertible, completion: @escaping (Swift.Result<[T], Error>) -> ()) {
        
        firstly {
            self.api.request(urlRequestConvertible: urlRequestConvertible)
        }.then { (data, response) in
            self.parser.printResponseJSON(data: data)
        }.then { data in
            self.parser.parse(type: [T].self, data: data)
        }.done { result in
            completion(.success(result))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func request(urlRequestConvertible: URLRequestConvertible, completion: @escaping (Swift.Result<[String: Any], Error>) -> ()) {
        
        firstly {
            self.api.request(urlRequestConvertible: urlRequestConvertible)
        }.then { (data, response) in
            self.parser.printResponseJSON(data: data)
        }.then { (data) in
            self.parser.parseToDictionary(data: data)
        }.done { result in
            completion(.success(result))
        }.catch { error in
            completion(.failure(error))
        }
    }
}
