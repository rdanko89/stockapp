//
//  AppDelegate.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 09..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit
@_exported import CollectionView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.startApp()
        return true
    }
    
    private func startApp() {
        let mainModule = MainModule().build()
        
        let mainNavigationController = UINavigationControllerFactory.mainNavigationController()
        mainNavigationController.viewControllers = [mainModule]
        mainNavigationController.navigationBar.prefersLargeTitles = true
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = mainNavigationController
        self.window?.makeKeyAndVisible()
    }
}
