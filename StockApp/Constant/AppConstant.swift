//
//  AppConstant.swift
//  StockApp Production
//
//  Created by Robert Danko on 2020. 07. 09..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

struct App {
    
    struct Colors {
        // https://coolors.co/000000-14213d-fca311-e5e5e5-ffffff
        static let primaryBackground = UIColor.black
        static let secondaryBackground = UIColor(red: 20, green: 33, blue: 61)!
        static let primary = UIColor.white
        static let secondary = UIColor(red: 229, green: 229, blue: 229)!
        static let base = UIColor(red: 252, green: 163, blue: 17)!
        static let separator = UIColor.white.withAlphaComponent(0.5)
        static let green = UIColor.green
        static let red = UIColor.red
    }
    
    struct Fonts {
        static let largePrimary = UIFont.systemFont(ofSize: 30)
        static let largePrimaryBold = UIFont.systemFont(ofSize: 30)
        static let primary = UIFont.systemFont(ofSize: 17)
        static let primaryBold = UIFont.boldSystemFont(ofSize: 17)
        static let secondary = UIFont.systemFont(ofSize: 14)
        static let secondaryBold = UIFont.boldSystemFont(ofSize: 14)
    }
    
    struct TimeIntervals {
        static let searchDebouncerInterval: TimeInterval = 0.75
    }
}
