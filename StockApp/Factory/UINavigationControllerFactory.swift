//
//  UINavigationControllerFactory.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 09..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class UINavigationControllerFactory {
    
    static func mainNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.navigationBar.barStyle = .black
        navigationController.navigationBar.tintColor = App.Colors.primary
        navigationController.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: App.Colors.primary]
        return navigationController
    }
}
