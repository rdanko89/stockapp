//
//  Debouncer.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class Debouncer {
    
    private let delay: TimeInterval
    private var timer: Timer?

    var handler: () -> Void

    init(delay: TimeInterval, handler: @escaping () -> Void) {
        self.delay = delay
        self.handler = handler
    }

    func call() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: delay, repeats: false, block: { [weak self] _ in
            self?.handler()
        })
    }

    func invalidate() {
        timer?.invalidate()
        timer = nil
    }
}
