//
//  JSONModel.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol JSONModel: Codable {
    static var decodingStrategy: JSONDecoder.KeyDecodingStrategy { get }
    static var encodingStrategy: JSONEncoder.KeyEncodingStrategy { get }
}

extension JSONModel {
    static var decodingStrategy: JSONDecoder.KeyDecodingStrategy { .useDefaultKeys }
    static var encodingStrategy: JSONEncoder.KeyEncodingStrategy { .useDefaultKeys }
}

extension Encodable {
    
    func asDictionary(keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy = .useDefaultKeys) -> [String: Any] {
        
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = keyEncodingStrategy
        
        guard let data = try? encoder.encode(self) else {
            
            return [String: Any]()
        }
        
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] ?? [String: Any]() } ?? [String: Any]()
    }
}
