//
//  JSONParser.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import PromiseKit

class JSONParser {
    
    func parse<T: JSONModel>(type: T.Type, data: Data) -> Promise<T> {
        
        return Promise<T> { seal in
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = T.decodingStrategy
            do {
                let result = try decoder.decode(T.self, from: data)
                seal.fulfill(result)
            } catch {
                seal.reject(APIError.jsonParseFailure)
            }
        }
    }
    
    func parse<T: JSONModel>(type: [T].Type, data: Data) -> Promise<[T]> {
        
        return Promise<[T]> { seal in
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = T.decodingStrategy
            do {
                let result = try decoder.decode([T].self, from: data)
                seal.fulfill(result)
            } catch {
                seal.reject(APIError.jsonParseFailure)
            }
        }
    }
    
    func parseToDictionary(data: Data) -> Promise<[String: Any]> {
        
        return Promise<[String: Any]> { seal in
            
            do {
                if let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    seal.fulfill(jsonDictionary)
                    return
                }
                seal.reject(APIError.jsonParseFailure)
            } catch {
                seal.reject(error)
            }
        }
    }
    
    /// Use before parsing the requested object. Best for parsing custom error from backend.
    func parseMiddleware<T: JSONModel>(type: T.Type, data: Data) -> Promise<Data> {
        
        return Promise<Data> { seal in
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = T.decodingStrategy
            do {
                let result = try decoder.decode(T.self, from: data)
                seal.reject(APIError.customError(error: result))
            } catch {
                seal.fulfill(data)
            }
        }
    }
    
    // For debug purposes
    func printResponseJSON(data: Data) -> Guarantee<Data> {
        
        return Guarantee<Data> { seal in
        
            do {
                let testResponsePrint = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print(testResponsePrint)
            } catch {
                print(error)
            }
            seal(data)
        }
    }
}
