//
//  SymbolSearchResult.swift
//  StockApp Develop
//
//  Created by Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

struct SymbolSearchResult: JSONModel, Hashable {
    
    let bestMatches: [Company]?
}
