//
//  MainDefaultInteractor.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class MainDefaultInteractor {

    weak var presenter: MainPresenter?
    
    private var mockData = MockData.mockCompanies()
    private let symbolSearchAPI = SymbolSearchAPI()
    private var companies: [Company] = [] {
        didSet {
            presenter?.companies(companies)
        }
    }
}

extension MainDefaultInteractor: MainInteractor {
    
    func getDefaultData() {
        companies = self.mockData
    }
    
    func searchWithKeyword(keyword: String) {
        symbolSearchAPI.symbolSearchWithKeyword(keyword) { [weak self] result in
            switch result {
            case .success(let symbolSearchResult):
                self?.companies = symbolSearchResult.bestMatches ?? []
            case .failure(let error):
                self?.presenter?.error(error)
            }
        }
    }
}
