//
//  MainInteractor.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol MainInteractor {

    var presenter: MainPresenter? { get set }
    
    func getDefaultData()
    func searchWithKeyword(keyword: String)
}
