//
//  MainModule.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class MainModule {
    
    func build() -> UIViewController {
        let view = MainDefaultView()
        let interactor = MainDefaultInteractor()
        let presenter = MainDefaultPresenter()
        let router = MainDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router

        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view
        
        return view
    }
}
