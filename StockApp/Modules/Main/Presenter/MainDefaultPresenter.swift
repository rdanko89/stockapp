//
//  MainDefaultPresenter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class MainDefaultPresenter {

    var router: MainRouter?
    var interactor: MainInteractor?
    weak var view: MainView?
    
    private func stocksSection(companies: [Company]) -> Section {
        let grid = Grid(columns: 1)
        
        let stockModels = companies.map { company in
            SubtitleModel(company: company)
        }
        
        let stockViewModels = stockModels.map { stockModel in
            SubtitleViewModel(stockModel).onSelect { [weak self] viewModel in
                self?.router?.showStockDetails(company: viewModel.model.company)
            }
        }
        
        return Section(grid: grid, items: stockViewModels)
    }
}

extension MainDefaultPresenter: MainPresenter {

    func showStockDetails(company: Company) {
        router?.showStockDetails(company: company)
    }
    
    func getDefaultData() {
        interactor?.getDefaultData()
    }
    
    func searchWithKeyword(keyword: String) {
        interactor?.searchWithKeyword(keyword: keyword)
    }
    
    func companies(_ companies: [Company]) {
        if companies.isEmpty {
            view?.setupWithSource(nil)
            return
        }
        let grid = Grid(columns: 1)
        let stocksSection = self.stocksSection(companies: companies)
        let source = Source(grid: grid, sections: [stocksSection])
        view?.setupWithSource(source)
    }
    
    func error(_ error: Error) {
        view?.error(error)
    }
}
