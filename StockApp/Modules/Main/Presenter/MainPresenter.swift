//
//  MainPresenter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol MainPresenter: class {

    var router: MainRouter? { get set }
    var interactor: MainInteractor? { get set }
    var view: MainView? { get set }
    
    func showStockDetails(company: Company)
    
    func getDefaultData()
    
    func searchWithKeyword(keyword: String)
    func companies(_ companies: [Company])
    func error(_ error: Error)
}
