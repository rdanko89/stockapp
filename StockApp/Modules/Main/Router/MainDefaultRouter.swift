//
//  MainDefaultRouter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class MainDefaultRouter {

    weak var presenter: MainPresenter?
    weak var viewController: UIViewController?
}

extension MainDefaultRouter: MainRouter {

    func showStockDetails(company: Company) {
        let stockDetailModule = StockDetailModule().buildWithCompany(company)
        viewController?.navigationController?.pushViewController(stockDetailModule, animated: true)
    }
}
