//
//  MainRouter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol MainRouter {

    var presenter: MainPresenter? { get set }
    
    func showStockDetails(company: Company)
}
