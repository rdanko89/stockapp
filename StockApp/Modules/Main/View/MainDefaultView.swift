//
//  MainDefaultView.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class MainDefaultView: CollectionViewController {

    var presenter: MainPresenter?
    
    private weak var activityIndicator: UIActivityIndicatorView!
    private weak var errorLabel: UILabel!
    private var searchDebouncer: Debouncer?
    private var currentKeyword = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupStyle()
        setupActivityIndicator()
        setupErrorLabel()
        setupDefaultData()
        setupSearchController()
        setupKeyboardNotification()
    }
    
    private func setupKeyboardNotification() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSizeValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardSize = keyboardSizeValue.cgRectValue.size
            collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        collectionView.contentInset = .zero
    }
    
    private func searchWithKeyword(_ keyword: String) {
        currentKeyword = keyword
        activityIndicator.startAnimating()
        collectionView.isHidden = true
        errorLabel.isHidden = true
        presenter?.searchWithKeyword(keyword: keyword)
    }
    
    private func setupDefaultData() {
        presenter?.getDefaultData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MainDefaultView: MainView {

    func setupWithSource(_ source: Source?) {
        activityIndicator.stopAnimating()
        let isSourceEmpty = source == nil
        if isSourceEmpty {
            errorLabel.text = "No result."
        }
        errorLabel.isHidden = !isSourceEmpty
        collectionView.isHidden = isSourceEmpty
        collectionView.source = source
        collectionView.reloadData()
    }
    
    func error(_ error: Error) {
        activityIndicator.stopAnimating()
        errorLabel.text = "Check your connection."
        errorLabel.isHidden = false
    }
}

extension MainDefaultView: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text, !searchText.isEmpty else {
            currentKeyword = ""
            searchDebouncer?.invalidate()
            setupDefaultData()
            return
        }
        
        if currentKeyword == searchText { return }
        
        let debounceHandler: () -> Void = { [weak self] in
            self?.searchWithKeyword(searchText)
        }
        
        guard let searchDebouncer = self.searchDebouncer else {
            
            let debouncer = Debouncer(delay: App.TimeIntervals.searchDebouncerInterval, handler: {})
            self.searchDebouncer = debouncer
            updateSearchResults(for: searchController)
            return
        }

        searchDebouncer.invalidate()
        searchDebouncer.handler = debounceHandler
        searchDebouncer.call()
    }
}

// Static view setups
extension MainDefaultView {
    
    private func setupNavigationBar() {
        title = "Stocks"
    }
    
    private func setupStyle() {
        view.backgroundColor = App.Colors.primaryBackground
    }
    
    private func setupActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .white
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator = activityIndicator
        view.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func setupErrorLabel() {
        let errorLabel = UILabel()
        errorLabel.text = "No result."
        errorLabel.textAlignment = .center
        errorLabel.font = App.Fonts.secondaryBold
        errorLabel.textColor = App.Colors.secondary
        errorLabel.isHidden = true
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        self.errorLabel = errorLabel
        view.addSubview(errorLabel)
        
        NSLayoutConstraint.activate([
            errorLabel.centerYAnchor.constraint(equalTo: activityIndicator.centerYAnchor),
            errorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func setupSearchController() {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
}
