//
//  MainView.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol MainView: class {

    var presenter: MainPresenter? { get set }
    
    func setupWithSource(_ source: Source?)
    func error(_ error: Error)
}
