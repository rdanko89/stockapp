//
//  StockDetailDefaultInteractor.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

class StockDetailDefaultInteractor {

    weak var presenter: StockDetailPresenter?
    
    private let globalQuoteAPI = GlobalQuoteAPI()
    private var globalQuote: GlobalQuote? {
        didSet {
            guard let globalQuote = globalQuote else {
                let customError = CustomError(message: "There was an error during the response. Please try again.")
                presenter?.error(APIError.customError(error: customError))
                return
            }
            presenter?.globalQuote(globalQuote)
        }
    }
}

extension StockDetailDefaultInteractor: StockDetailInteractor {
    
    func getGlobalQuoteWithSymbol(_ symbol: String) {
        globalQuoteAPI.getGlobalQuoteWithSymbol(symbol) { [weak self] result in
            switch result {
            case .success(let globalQuoteResult):
                self?.globalQuote = globalQuoteResult.globalQuote
            case .failure(let error):
                self?.presenter?.error(error)
            }
        }
    }
}
