//
//  StockDetailInteractor.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol StockDetailInteractor {

    var presenter: StockDetailPresenter? { get set }
    
    func getGlobalQuoteWithSymbol(_ symbol: String)
}
