//
//  StockDetailDefaultPresenter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class StockDetailDefaultPresenter {
    
    var router: StockDetailRouter?
    var interactor: StockDetailInteractor?
    weak var view: StockDetailView?
    
    var company: Company?
    
    private var separatorViewModel: SeparatorViewModel {
        return SeparatorViewModel(SeparatorModel())
    }
    private var collectionInsets: UIEdgeInsets {
        return UIEdgeInsets(horizontal: 8, vertical: 0)
    }
    
    private func stockPriceSection(globalQuote: GlobalQuote) -> Section {
        let grid = Grid(columns: 1, margin: collectionInsets)
        
        let stockPriceModels = [
            TitleDetailModel(title: "Open", price: globalQuote.open, priceColor: App.Colors.base),
            TitleDetailModel(title: "High", price: globalQuote.high, priceColor: App.Colors.green),
            TitleDetailModel(title: "Low", price: globalQuote.low, priceColor: App.Colors.red)
        ]
        
        var stockPriceItems: [ViewModelProtocol] = stockPriceModels.map { stockPrice in
            TitleDetailViewModel(stockPrice)
        }
        
        stockPriceItems.append(separatorViewModel)
        return Section(grid: grid, items: stockPriceItems)
    }
    
    private func changeSection(globalQuote: GlobalQuote) -> Section {
        let grid = Grid(columns: 2, margin: collectionInsets)
        
        let changeModels = [
            ChangeModel(title: "Change", change: globalQuote.change),
            ChangeModel(title: "Change in %", change: globalQuote.changePercent)
        ]
        
        var changeModelsItems: [ViewModelProtocol] = changeModels.map { changeModel in
            ChangeViewModel(changeModel)
        }
        
        changeModelsItems.append(separatorViewModel)
        
        return Section(grid: grid, items: changeModelsItems)
    }
    
    private func latestTradingDaySection(globalQuote: GlobalQuote) -> Section {
        let grid = Grid(columns: 1, margin: collectionInsets)
        
        let changeModels = [
            TitleDetailModel(title: "Lastest Trading Day", price: globalQuote.latestTradingDay, priceColor: App.Colors.primary)
        ]
        
        var changeModelsItems: [ViewModelProtocol] = changeModels.map { changeModel in
            TitleDetailViewModel(changeModel)
        }
        
        changeModelsItems.append(separatorViewModel)
        
        return Section(grid: grid, items: changeModelsItems)
    }
}

extension StockDetailDefaultPresenter: StockDetailPresenter {
    
    func viewDidLoad() {
        if let symbol = company?.symbol {
            interactor?.getGlobalQuoteWithSymbol(symbol)
            view?.setupTitle(symbol)
        }
    }
    
    func globalQuote(_ globalQuote: GlobalQuote) {
        let grid = Grid(columns: 4)
        let stockPriceSection = self.stockPriceSection(globalQuote: globalQuote)
        let changeSection = self.changeSection(globalQuote: globalQuote)
        let latestTradingDaySection = self.latestTradingDaySection(globalQuote: globalQuote)
        let source = Source(grid: grid, sections: [stockPriceSection, changeSection, latestTradingDaySection])
        view?.setupWithSource(source)
    }
    
    func error(_ error: Error) {
        view?.error(error)
    }
}
