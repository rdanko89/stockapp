//
//  StockDetailPresenter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol StockDetailPresenter: class {

    var router: StockDetailRouter? { get set }
    var interactor: StockDetailInteractor? { get set }
    var view: StockDetailView? { get set }
    
    func viewDidLoad()
    
    func globalQuote(_ globalQuote: GlobalQuote)
    func error(_ error: Error)
}
