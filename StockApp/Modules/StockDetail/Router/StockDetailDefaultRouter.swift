//
//  StockDetailDefaultRouter.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class StockDetailDefaultRouter {

    weak var presenter: StockDetailPresenter?
    weak var viewController: UIViewController?
}

extension StockDetailDefaultRouter: StockDetailRouter {

    
}
