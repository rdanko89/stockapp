//
//  StockDetailModule.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class StockDetailModule {

    func buildWithCompany(_ company: Company) -> UIViewController {
        let view = StockDetailDefaultView()
        let interactor = StockDetailDefaultInteractor()
        let presenter = StockDetailDefaultPresenter()
        let router = StockDetailDefaultRouter()

        view.presenter = presenter

        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        presenter.company = company
        
        interactor.presenter = presenter

        router.presenter = presenter
        router.viewController = view

        return view
    }
}
