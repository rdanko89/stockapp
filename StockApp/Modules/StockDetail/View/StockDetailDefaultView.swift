//
//  StockDetailDefaultView.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

class StockDetailDefaultView: CollectionViewController {
    
    var presenter: StockDetailPresenter?
    
    private weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        setupNavigationBar()
        setupStyle()
        setupActivityIndicator()
    }
}

extension StockDetailDefaultView: StockDetailView {
    
    func setupTitle(_ title: String?) {
        self.title = title
    }
    
    func setupWithSource(_ source: Source) {
        activityIndicator.stopAnimating()
        collectionView.source = source
        collectionView.reloadData()
    }
    
    func error(_ error: Error) {
        activityIndicator.stopAnimating()
        let alertController = UIAlertController(title: "Error", message: "There was an error during the response. Please try again.", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default) { [weak self] alertAction in
            self?.navigationController?.popViewController(animated: true)
        }
        
        alertController.addAction(alertAction)
        present(alertController, animated: true)
    }
}

// Static view setups
extension StockDetailDefaultView {
    
    private func setupNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupStyle() {
        view.backgroundColor = App.Colors.primaryBackground
    }
    
    private func setupActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .white
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator = activityIndicator
        view.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
