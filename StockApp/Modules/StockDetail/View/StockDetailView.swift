//
//  StockDetailView.swift
//  StockApp
//
//  Created Robert Danko on 2020. 07. 10..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

protocol StockDetailView: class {

    var presenter: StockDetailPresenter? { get set }
    
    func setupTitle(_ title: String?)
    func setupWithSource(_ source: Source)
    func error(_ error: Error)
}
