//
//  ChangeCell.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class ChangeCell: Cell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.font = App.Fonts.primary
        titleLabel.textColor = App.Colors.primary
        
        changeLabel.font = App.Fonts.primaryBold
        changeLabel.textColor = App.Colors.primary
    }
    
    override func reset() {
        super.reset()
        
        titleLabel.text = nil
        changeLabel.text = nil
    }
}
