//
//  ChangeModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation

struct ChangeModel {
    let title: String
    let change: String?
}
