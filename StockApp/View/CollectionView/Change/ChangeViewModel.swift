//
//  ChangeViewModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class ChangeViewModel: ViewModel<ChangeCell, ChangeModel> {
    
    override var height: CGFloat { 70 }
    
    override func updateView() {
        view?.titleLabel.text = model.title
        view?.changeLabel.text = model.change
    }
    
    override func size(grid: Grid) -> CGSize {
        return grid.size(for: collectionView, height: self.height, items: grid.columns / 2)
    }
}
