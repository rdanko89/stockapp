//
//  SeparatorCell.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class SeparatorCell: Cell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func reset() {
        super.reset()
    }
}
