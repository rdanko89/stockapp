//
//  SeparatorModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

struct SeparatorModel {
    let color = App.Colors.separator
}
