//
//  SeparatorViewModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class SeparatorViewModel: ViewModel<SeparatorCell, SeparatorModel> {

    override var height: CGFloat { 1 }

    override func updateView() {
        self.view?.contentView.backgroundColor = self.model.color
    }

    override func size(grid: Grid) -> CGSize {
        return grid.size(for: collectionView, height: self.height, items: grid.columns)
    }
}
