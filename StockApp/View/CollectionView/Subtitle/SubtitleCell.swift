//
//  SubtitleCell.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 12..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class SubtitleCell: Cell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.font = App.Fonts.primaryBold
        titleLabel.textColor = App.Colors.base
        
        subtitleLabel.font = App.Fonts.secondary
        subtitleLabel.textColor = App.Colors.secondary
        
        rightArrowImageView.tintColor = App.Colors.base
    }
    
    override func reset() {
        super.reset()
        
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
}
