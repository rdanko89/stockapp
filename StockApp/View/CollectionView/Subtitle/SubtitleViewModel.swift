//
//  SubtitleViewModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 12..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class SubtitleViewModel: ViewModel<SubtitleCell, SubtitleModel> {
    
    override var height: CGFloat { 70 }
    
    override func updateView() {
        view?.titleLabel.text = model.company.symbol
        view?.subtitleLabel.text = model.company.name
    }
}
