//
//  TitleDetailCell.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class TitleDetailCell: Cell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.font = App.Fonts.primary
        titleLabel.textColor = App.Colors.primary
        
        priceLabel.font = App.Fonts.primaryBold
    }
    
    override func reset() {
        super.reset()
        
        titleLabel.text = nil
        priceLabel.text = nil
    }
}
