//
//  TitleDetailModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import Foundation
import UIKit

struct TitleDetailModel {
    let title: String
    let price: String?
    let priceColor: UIColor
}
