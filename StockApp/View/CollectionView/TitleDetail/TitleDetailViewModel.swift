//
//  TitleDetailViewModel.swift
//  StockApp
//
//  Created by Robert Danko on 2020. 07. 11..
//  Copyright © 2020. Robert Danko. All rights reserved.
//

import UIKit

class TitleDetailViewModel: ViewModel<TitleDetailCell, TitleDetailModel> {
    
    override var height: CGFloat { 44 }
    
    override func updateView() {
        self.view?.titleLabel.text = self.model.title
        self.view?.priceLabel.text = self.model.price
        self.view?.priceLabel.textColor = self.model.priceColor
    }
}
